﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    [Header("General")]
    [Tooltip("In m/s")] [SerializeField] float xSpeed = 24f;
    [Tooltip("In m")] [SerializeField] float xRange = 6f;
    [Tooltip("In m/s")] [SerializeField] float ySpeed = 14f;
    [Tooltip("In m")] [SerializeField] float yTopRange = 5f;
    [Tooltip("In m")] [SerializeField] float yBottomRange = 4.8f;
    [SerializeField] GameObject[] guns;

    [Header("Screen-posistion Based")]
    [SerializeField] float positionYawFactor = 5f;
    [SerializeField] float positionPitchFactor = -5f;

    [Header("Control-throw Based")]
    [SerializeField] float controlPitchFactor = -24f;
    [SerializeField] float controlRollFactor = -24f;

    float xThrow, yThrow;

    bool controlsEnabled = true;

    // Update is called once per frame
    void Update()
    {
        if (controlsEnabled)
        {
            updateX();
            updateY();
            processRotation();
            processFiring();
        }
        
    }

    void processFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            SetGunsActive(true);
        } else
        {
            SetGunsActive(false);
        }   
    }

    private void SetGunsActive(bool b)
    {
        foreach (GameObject gun in guns)
        {
            var emissionModule = gun.GetComponent<ParticleSystem>().emission;
            emissionModule.enabled = b;
        }
    }

    private void updateX()
    {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        float xOffset = xThrow * xSpeed * Time.deltaTime;
        float rawXPos = transform.localPosition.x + xOffset;
        float clampledXPos = Mathf.Clamp(rawXPos, -xRange, xRange);
        transform.localPosition = new Vector3(clampledXPos,
                                  transform.localPosition.y,
                                  transform.localPosition.z);
    }

    private void updateY()
    {
        yThrow = CrossPlatformInputManager.GetAxis("Vertical");
        float yOffset = yThrow * ySpeed * Time.deltaTime;
        float rawYPos = transform.localPosition.y + yOffset;
        float clampledYPos = Mathf.Clamp(rawYPos, -yBottomRange, yTopRange);
        transform.localPosition = new Vector3(transform.localPosition.x,
                                  clampledYPos,
                                  transform.localPosition.z);
    }

    private void processRotation()
    {
        float pitch = transform.localPosition.y 
                      * positionPitchFactor 
                      + yThrow 
                      * controlPitchFactor;
        float yaw = transform.localPosition.x * positionYawFactor;
        float roll = xThrow * controlRollFactor;
        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    void OnPlayerDeath()
    {
        controlsEnabled = false;
    }
    
}
